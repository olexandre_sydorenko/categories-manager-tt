<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            [
                'name' => 'Category 1',
            ],
            [
                'name' => 'Category 2',
                'children' => [
                    [
                        'name' => 'Category 3'
                    ],
                    [
                        'name' => 'Category 4'
                    ],
                ]
            ],
            [
                'name' => 'Category 5',
            ],
            [
                'name' => 'Category 6',
            ],
            [
                'name' => 'Category 7',
                'children' => [
                    [
                        'name' => 'Category 8'
                    ],
                    [
                        'name' => 'Category 9'
                    ],
                ]
            ],
        ];

        $this->create($categories);
    }

    public function create(array $categories, int $parentId = null): void
    {
        foreach ($categories as $category) {
            $parent = Category::create([
                'name' => $category['name'],
                'parent_id' => $parentId,
            ]);

            if (isset($category['children'])) {
                $this->create($category['children'], $parent->id);
            }
        }
    }
}
