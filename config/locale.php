<?php

return [
    'default' => 'en',
    'languages' => [
        [
            'code' => 'en',
            'name' => 'EN'
        ],
        [
            'code' => 'ua',
            'name' => 'UA'
        ],
    ]
];
