<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    public function __construct(
        protected readonly UserRepository $userRepository
    )
    {
    }

}
