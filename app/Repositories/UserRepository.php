<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function __construct(
        protected readonly User $model
    ) {}

    public function getUsers(array $data = []): array
    {
        return $this->model->newQuery()
            ->limit($data['limit'] ?? null)
            ->get()->toArray();
    }

    public function create(array $data): User
    {
        return $this->model->newQuery()->create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password']
            ]);
    }
}
