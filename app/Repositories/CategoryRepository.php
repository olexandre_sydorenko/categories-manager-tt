<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\CategoryTranslation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\ValidationException;

class CategoryRepository
{
    public function __construct(
        protected readonly Category $model,
        protected readonly CategoryTranslation $categoryTranslationModel
    ) {}

    public function getCategories(): Collection
    {
        return $this->model->newQuery()
            ->with(['parent' => function ($query) {
                $query->with(['translation']);
            }, 'translation'])
            ->get();
    }

    public function updateCategories(array $data): void
    {
        foreach ($data['items'] as $item) {
            $this->categoryTranslationModel->newQuery()
                ->updateOrCreate([
                    'category_id' => $item['id'],
                    'lang' => $data['lang']
                ], [
                    'translation' => $item['translation'] ?? ''
                ]);
        }
    }
}
