<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'lang' => 'required|string',
            'items' => 'required|array',
            'items.*.id' => 'required|int',
            'items.*.translation' => 'string|nullable'
        ];
    }
}
