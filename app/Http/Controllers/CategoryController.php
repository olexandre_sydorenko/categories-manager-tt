<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public function __construct(
        protected readonly CategoryRepository $categoryRepository
    )
    {
    }

    public function index(): View
    {
        $categories = $this->categoryRepository->getCategories();
        return view('category.index', [
            'languages' => config('locale.languages'),
            'lang' => App::currentLocale(),
            'categories' => $categories
        ]);
    }

    public function update(UpdateCategoryRequest $request): RedirectResponse
    {
        $this->categoryRepository->updateCategories($request->validated());
        return back();
    }
}
