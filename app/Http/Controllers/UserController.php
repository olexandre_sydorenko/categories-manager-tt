<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\IndexRequest;
use App\Http\Requests\User\StoreRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;

class UserController
{
    public function __construct(protected readonly UserRepository $userRepository)
    {
    }

    public function index(IndexRequest $request): JsonResponse
    {
        $users = $this->userRepository->getUsers($request->validated());
        return response()->json($users);
    }

    public function store(StoreRequest $storeRequest): JsonResponse
    {
        $this->userRepository->create($storeRequest->validated());
        return response()->json();
    }
}
