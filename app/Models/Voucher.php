<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $code
 * @property int $discount
 */
class Voucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'discount',
        'code'
    ];
}
